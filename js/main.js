$(function() {
    var pull = $('.nav-toggle');
    var menu = $('.nav');

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('nav-open');
    });
});

$(function($){
    var point = $('.nav');
    var label = $('.nav');
    $h = label.offset().top;

    $(window).scroll(function(){

        if ( $(window).scrollTop() > $h) {
            point.addClass('fix-top');
        }else{
            //Иначе возвращаем всё назад. Тут вы вносите свои данные
            point.removeClass('fix-top');
        }
    });
});




$(".btn-modal").fancybox({
    'padding'    : 0
});


var promoSlider = new Swiper('.promoSlider', {
    loop: true,
 /*   autoplay: {
        delay: 10000,
        disableOnInteraction: false
    },*/
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    }
});



var partners = new Swiper('.partners-slider', {
    loop: true,
    slidesPerView: 4,
    spaceBetween: 15,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    breakpoints: {
        1060: {
            slidesPerView: 3
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 15
        },
        768: {
            slidesPerView: 1,
            spaceBetween: 20
        }
    }
});


var calendar = new Swiper('.calendar-slider', {
    slidesPerView: 7,
    spaceBetween: 8,
    grabCursor: true,
    pagination: {
        el: '.swiper-pagination',
        type: 'progressbar'
    },
    navigation: {
        nextEl: '.calendar-button-next',
        prevEl: '.calendar-button-prev'
    },
    breakpoints: {

        1060: {
            slidesPerView: 7

        },

        992: {
            slidesPerView:5

        },

        768: {
            slidesPerView: 3

        },

        520: {
            slidesPerView:3

        },

        320: {
            slidesPerView: 2

        }
    }
});


var day = new Swiper('.day-slider', {
    slidesPerView: 8,
    grabCursor: true,
    loop: true,
    navigation: {
        nextEl: '.day-button-next',
        prevEl: '.day-button-prev'
    },
    breakpoints: {

        1060: {
            slidesPerView: 7

        },

        992: {
            slidesPerView:5

        },

        768: {
            slidesPerView: 3

        },

        520: {
            slidesPerView:3

        },

        320: {
            slidesPerView: 2

        }
    }
});

(function() {

    $('.schedule__date').on('click touchstart', function(e){
        e.preventDefault();

        var box = $(this).closest('.tabs');

        $(this).closest('.schedule__calendar').find('.schedule__date').removeClass('active');
        $(this).addClass('active');
    });

}());

// Time

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getCurrentTime(){
    var time, h, m;
    time = new Date(Date.now());
    h = time.getUTCHours() + 3;
    m = addZero(time.getUTCMinutes());



    if (h >= 8 && h <= 20) {

        console.log('открыто');
        $('.worktime__status').removeClass('closed');
    }
    else {
        console.log('закрыто');
        $('.worktime__status').addClass('closed');
    }
}


getCurrentTime();

time100.init({
    timenow:{zone:"Europe/Moscow",format:"%H:%i"}
});